
import { configure } from '@storybook/html';

import { configureStorybook }  from '../storybook-config-helper';

import theme from './common-theme';


configureStorybook(theme);

// `require.context` is parsed by WebPack and the path must be defined statically, i.e. hard-coded
configure(require.context('../../src/_common', true, /\.stories.js$/), module);