import {getThemeConfig} from '../storybook-webpack-config-helper';

module.exports = async ({config, mode}) => {
    return getThemeConfig('common', config);
};