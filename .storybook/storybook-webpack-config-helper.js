import customConfig from '../config/webpack.base.config';
import settings from "../config/settings";
import webpack from 'webpack';

export const getThemeConfig = async (themeName, baseConfig) => {

    const theme = settings.themes.find(x => x.key === themeName);

    // Get the entry points from our custom config for the current theme and append it to the base config
    let entry = Object.keys(customConfig.entry)
    // Find entry points that match our theme
        .filter(x => x.indexOf(theme.key) === 0)
        // Combine Storybook's entry with ours
        .reduce((obj, key) => ({
                ...obj,
                // For unknown reasons, build-storybook fails if entry point name has a pipe symbol.
                // It works just fine in in start-storybook, as well as in dev and prod builds normally.
                [key.replace('|', '-')]: customConfig.entry[key]
            }),
            { main: baseConfig.entry });

    return {
        ...baseConfig,
        entry: entry,
        resolve: {
            ...baseConfig.resolve,
            ...customConfig.resolve
        },
        module: {
            ...baseConfig.module,
            rules: [
                ...customConfig.module.rules,
                // Needed because storybook loads the css inline
                {
                    test: /\.s?css$/i,
                    exclude: /(node_modules)/,
                    enforce: 'post', // This needs to be called after the loaders from the base config
                    use: [
                        { loader: 'style-loader' },
                    ]
                }, {
                    test: /\.md$/,
                    use: [
                        {
                            loader: "html-loader"
                        },
                        {
                            loader: "markdown-loader",
                            options: {}
                        }
                    ]
                }
            ]
        },
        optimization: {
            ...baseConfig.optimization,
            runtimeChunk: 'single'
        },
        plugins: [
            ...baseConfig.plugins,
            // Expose jQuery to the global scope
            // Prefer to import jQuery explicitly as it can guarantee no-conflict is applied
            // This exists for any external JS that might need jQuery
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                // Some jQuery plugins look for jQuery on the window object, provide it.
                'window.$': 'jquery',
                'window.jQuery': 'jquery',
            }),
        ]
    }
};