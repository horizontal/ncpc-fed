let initialBodyClass = null;

export const withBodyClass = (story, options) => {
    // Cache the initial body class list
    if (initialBodyClass === null) {
        initialBodyClass = [...document.body.classList];
    }
    // Clear all classes
    document.body.className = "";
    // Re-add the original ones
    document.body.classList.add(...initialBodyClass);
    // If a body class was passed in as an option, add it
    if (options.parameters.bodyClass) {
        document.body.classList.add(options.parameters.bodyClass);
    }
    
    return story();
}