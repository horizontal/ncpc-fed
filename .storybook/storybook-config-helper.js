import { addParameters, addDecorator } from '@storybook/html';
import { withKnobs } from '@storybook/addon-knobs';
import { withA11y } from '@storybook/addon-a11y';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { addReadme } from 'storybook-readme/html';
import { withBodyClass } from './addons/body-class';

// Styles only used in storybook and not on the actual site
import './storybook-style-helper.scss';


export const configureStorybook = function (theme) {
    const rootOrder = [
        'Styleguide', 'Elements', 'Components', 'Pages'
    ]
    addParameters({
        bodyClass: null, // body class can be set globally, or on a story or module level
        options: {
            theme: theme,
            storySort: (x, y) => rootOrder.indexOf(x[1].kind.split('|')[0]) - rootOrder.indexOf(y[1].kind.split('|')[0])
        },
        a11y: {
            // axe-core optionsParameter (https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#options-parameter)
            options: {

                runOnly: {
                    type: 'tag',
                    values: ['section508', 'wcag2aa']
                }
            }
        },
    });


    addParameters({
        storybookCodePanel: {
            // Disable by default, enable per story
            disabled: true,
            // For file extensions that don't match Prism language name, map file extension to language
            // See list at: https://prismjs.com/#languages-list
            extensionMapping: {
                hbs: 'handlebars'
            },
            // Optional, when auto-adding files specify allowed file extensions and the order to display
            allowedExtensions: [
                'hbs', 'scss', 'css','js', 'json'
            ]
        }
    });

    addParameters({
        viewport: {
            viewports: INITIAL_VIEWPORTS,
        },
    });

    addDecorator(addReadme);

    addDecorator(withKnobs({
        escapeHTML: false
    }));

    addDecorator(withA11y);

    addDecorator(withBodyClass);
}

export const combineWebpackContexts = (...allContexts) => {
    function webpackContext(req) {
        // Find the first match and execute
        const matches = allContexts
            .map(context => context.keys().indexOf(req) >= 0 && context)
            .filter(a => a);

        return matches[0] && matches[0](req);
    }
    webpackContext.keys = () => {
        let allKeys = allContexts.reduce((curr, context) => [...curr, ...context.keys()], [])
        return [...new Set(allKeys)];
    };

    return webpackContext;
};
