import { configure } from '@storybook/html';

import { configureStorybook, combineWebpackContexts } from '../storybook-config-helper';

import theme from './brand-1-theme';


configureStorybook(theme);

// `require.context` is parsed by WebPack and the path must be defined statically, i.e. hard-coded
const brand1Context = require.context('../../src/brand-1', true, /\.stories.js$/);
const commonContext = require.context('../../src/_common', true, /\.stories.js$/);

// Include stories from brand-1 and common, whichever comes first
const allContexts = combineWebpackContexts(brand1Context, commonContext);

configure(allContexts, module);
