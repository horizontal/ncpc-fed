# HIVE Setup Guide
Setting up for the first time

## Prerequisites
Ensure you have the correct version of Node.js installed.  Check under engines>node in package.json for the version requirement.

### Node Version Manager
Windows: 
Download and install the latest version of [NVM for Windows](https://github.com/coreybutler/nvm-windows/releases) 
Note: If there are errors installing a specified version of Node, try installing the latest NVM.  There have been breaking changes in the way the remote downloads were structured.

Mac/Linux: 
Install NVM according to instructions on their [GitHub repo](https://github.com/nvm-sh/nvm)

All:
Install the correct version of node.  
If you have a previous version of node installed already you can run `npm run update-node`.  
* Install the correct version of Node.
* Use the correct version of Node.

If Node has never been installed, open `package.json` and copy the command in the `update-node` script and run it directly.

If changing Node versions after previously called `npm install`:
* Call `npm run fix-node-sass` (or `npm rebuild node-sass`).  `fix-node-sass` was added as a script so it shows in the Visual Studio UI.  All it does is call `npm rebuild node-sass` which can be called directly.
* Call `npm install` to ensure packages are compatible.


## Building
To build the project, first run `npm install` to ensure all dependencies are installed.  
This should be run any time there are dependency changes, so it is good to run it when 
when you pull down changes.  If you run into errors, running `npm install` to rule that out.

Note: If you change node versions, you will need to re-run `npm-install`

### Development (BrowserSync)
Running `npm run dev` will build in dev mode and start BrowserSync on port 3000.  
After running, go to [http://localhost:3000/common] to view the site.  

The default output path is `public/dev`

**NOTE** 
The preferred mode for debugging/developing is using Storybook.  
See STORYBOOK-README.md for details
Use `npm run dev` to ensure that things like the favicon or things in the `<head>` is set up correctly, but for the most part
storybook will be the preferred workflow for development.

You can also use Chrome's dev tools along with sourcemaps to ensure that only the expected JS libraries are included.

In Dev build, css is bundled but not minified to easily verify output

### Production
Running `npm run build` will build in prod mode, which will minify assets.

The default output path is `public/dist`


## Settings
In most projects, the only file that will need to be touched is `config/settings.js`

### Theming
The system is set up to support multiple themes.  
What a "Theme" represents can vary from project to project.
It can be used in a multi-site scenario where each site might have a different theme.
It can be used in a multi-brand scenario where each brand might have it's own styling.
It can be used in a scenario where different parts of a site might have visual variations
It can be used where one part of a site requires a significant amount of unique JS/CSS, including 3rd party librarires that are not needed in the rest of the site, and we don't want to introduce the bandwidth/memory/performance overhead of bringing in that code on every page

Sample:
```javascript
    themes: [
         {
            // Folder to be created inside `dest` and in `copyPath`.  
            // Set to empty string to not generate extra folder
            outputName: 'common', 
            
            // Source files for this theme
            srcPath: './src/_common', 
            
            // Path to copy output to in BED code.  Ignored if path doesn't exist
            copyPath: '../Horizontal.Sample.Common.Web/Assets',
            
            // If set to `true` the `copyPath` will be deleted prior to copying.  
            // Make sure path is correct and that assets are not directly added to copy path
            deleteCopyPath: false 
        },
        { outputName: 'brand-1', srcPath: './src/brand-1', copyPath: '../Horizontal.Sample.Project1.Web/Assets'  },
    ],
```
* In the above `outputName` refers to the subfolder that gets created for a theme.  
* The `srcPath` represents where the code for that theme is located.
* The `copyPath` represents where the code for that theme should be copied on production build.  
    * Note: The `outputName` will be appended to the `copyPath` so in the above files in 'common' will be copied to '../Horizontal.Sample.Common.Web/Assets/common'
    * Note: Shared files will be copied to the common theme (see below) *without* the output name. 
    * e.g. files shared between themes will be copied to '../Horizontal.Sample.Common.Web/Assets/' 

#### Common Theme
This represents the default theme.  Code should go here by default.  If there are no other themes, feel free to delete the `brand-1` sample theme under `src` and the corresponding entry in `config/settings.js` under `settings.theme`.  Also (optionally) set the output name of the `common` theme to `''` (empty string) so a subfolder does not get output.

Additional themes "inherit" from the common theme.

Under `src` by default the common theme is called `_common` so it shows up first in the list.  This is not strictly required.  If there is a good reason to rename it, simply changing the theme entry in `config/settings.js` will be sufficient for it to resolve correctly.

#### Additional Themes (sample: "brand-1")
If using multiple themes, rename "brand-1" folder to something appropriate for the project.  Also update the corresponding entries in `config/settings.js`. 
Copy/repeat for additional themes.

### Theming behavior
In general themes follow a fallback system.  What this means for different file types varies slightly.

#### HTML files
HTML files are rendered using [Handlebars](https://handlebars-draft.knappi.org/guide/).  See docs for additional details.
In a themed set up, when extending or including, it will first search in the current theme's folder, then the "common" theme folder.  
Pathing should be relative to theme, so extending layout would look like `{% extends 'html/layouts/default.html' %}` 
and including components would look like `{% include 'components/callout/callout.html' %}`
If that file exists in the current theme it will use it.  Otherwise it will look for that file in the common theme.

NOTE: Images and resources should be specified with an absolute path relative to the output root.     
e.g. `/brand-1/images/logo.png`  or `/common/fonts/JurassicPark/JurassicPark.ttf`  
**This is different from working in `scss` files where paths are relative to the source file.**

NOTE: Overriding HTML in themes will require additional work on the back end.  Discuss with BED team before proceeding.  
In most cases, the html files should only exist in the common theme.

#### (S)CSS files
CSS files are generated using [SCSS](https://sass-lang.com/documentation/syntax).  
By default, specific themes will include the following
```scss
// Our variable overrides.  Common variables should use !default so we can overrride variables
@import 'variables';

// Common styles
@import "../../_common/stylesheets/main";
``` 

The variables are included first to override variable declarations under common, as well as Bootstrap variables
Note: In the common theme, variables are specified with `!default` to allow overriding, e.g. `$color-link: #07C !default;`

In the common `main.scss` file, we have the following line: `@import '../components/**/*.scss';` with imports all component scss files.
This is supported due to a webpack plugin.  
This only needs to be specified in the common `main.scss` and not in the themed `main.scss`.

Normal CSS overriding rules apply.

Paths to fonts and images should be relative to the current source file.

Regarding Bootstrap, we recommend only pulling in the parts that are needed for your site.  
For example if you have custom button styles you wouldn't need Bootstrap's button styles.  
They can be imported by the specific component or element that needs them.
The CSS gets de-duped, so even if two components import the same CSS, it will only get output once in the final css. 


#### JS files
All component JS files are automatically pulled in and bundled via WebPack.  It is assumed that all component JS code is independent unless otherwise specified via `import` statements.  If there is shared JS code such as utility code, create a separate JS file and `export` the functionality to be `import`ed by components that need it.

An alias is set up for JS import for the common theme.  For example: `import SomeUtility from "common/components/util/some-utility";` from any theme will know to look under the common theme (even if the folder is renamed as long as the setting is correct).

By default, all component JS will be pulled from the common theme.  If there is a corresponding file with the same name in a theme, **it will override the functionality** in the common theme.  This should happen rarely.  If there is a need to simply augment the functionality, the code under the common theme should be structured in such a way that extension is possible.  

You can import the code from the common version (e.g. `import 'common/components/callout/callout';`) and add to it, or you can extract the functionality into a class in a separate file e.g. 

`common/components/callout/callout-class.js` 
```js
export default class Callout {
    init() {
        this.someFunction();
    }
    someFunction(){
        // stuff
    }
}
```

`common/components/callout/callout.js` 
```js
import Callout from 'callout-class.js';
let callout = new Callout();
callout.init();
```


`brand-1/components/callout/callout-class.js` 
```js
import Callout from 'common/components/callout/callout-class';

export default class ThemedCallout extends Callout {
    someFunction(){
        super.someFunction()
        // DO more stuff
    }
}
```


`brand-1/components/callout/callout.js` 
```js
import ThemedCallout from 'callout-class.js';
let callout = new ThemedCallout();
callout.init();
```