module.exports = {
    'env': {
        'browser': true,
        'es6': true
    },
    'extends': [
        'eslint:recommended',
        // 'plugin:import/errors',
        // 'plugin:import/warnings',
    ],
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },
    // 'settings': {
    //     'import/resolver': {
    //         webpack: {
    //             config: ['config/webpack.dev.config.js', 'config/webpack.prod.config.js']
    //         }
    //     }
    // },
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },
    'rules': {
        'indent': [
            'error',
            2
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        'no-unused-vars': ['warn', { "varsIgnorePattern": "^_", "argsIgnorePattern": "^_" }]
    }
};
