import Webpack from 'webpack';
import merge from 'webpack-merge';
import baseConfig from './webpack.base.config';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export default merge(baseConfig, {
    mode: 'production',
    devtool: 'source-map',
    bail: true,
    module: {
        rules: [
            {
                test: /\.s?css$/i,
                exclude: /(node_modules)/,
                enforce: 'post', // This needs to be called after the loaders from the base config
                use: [
                    // Extract CSS into actual CSS files since WebPack loads them as JS
                    {loader: MiniCssExtractPlugin.loader},
                ]
            }
        ],
    },
    plugins: [
        new Webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new Webpack.optimize.ModuleConcatenationPlugin(),
    ],
});