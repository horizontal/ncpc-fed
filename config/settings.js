// eslint-disable-next-line no-undef
const isProduction = (process.env.NODE_ENV === 'production');

// Where the files will be output to
const dest = isProduction ? 'public/dist/' : 'public/dev/';

// This should be configured based on the path of where assets are located in the back-end
const publicPath = isProduction ? '/Assets' : '/';

const settings = {
    isProduction: isProduction,
    dest: dest,
    publicPath: publicPath,


    themes: [
        {
            // Use this to lookup theme by name.
            key: 'common',

            // Folder to be created inside `dest` and in `copyPath`.  
            // Set to empty string to not generate extra folder
            outputName: 'common',

            // Source files for this theme
            srcPath: './src/_common',

            // Path to copy output to in BED code.  Ignored if path doesn't exist
            copyPath: '../Horizontal.Sample.Common.Web/Assets',

            // If set to `true` the `copyPath` will be deleted prior to copying.  
            // Make sure path is correct and that assets are not directly added to copy path
            deleteCopyPath: false
        },
        // Additional themes, if any.
        {
            key: 'brand-1',
            outputName: 'brand-1',
            srcPath: './src/brand-1',
            copyPath: '../Horizontal.Sample.Project1.Web/Assets',
            deleteCopyPath: false
        },
    ],
    get commonTheme() {
        // Assumes the common theme is the first one.  
        // This is used for various things such as:
        //      Path resolution in Nunjucks templates, 
        //      Import aliasing
        //      Component fallback. 
        //      Shared files will be copied to common theme
        return this.themes[0];
    },
};

export default settings;