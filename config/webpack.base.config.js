import webpack from 'webpack';

import path from 'path';
import glob from 'glob';

// Allows wildcard in scss imports
import sassGlobImporter from 'node-sass-glob-importer';

import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

// postcss plugins
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import IconfontWebpackPlugin from 'iconfont-webpack-plugin';

import settings from './settings';

const config = {

    devtool: 'source-map',
    resolve: {
        modules: ['node_modules'],
        alias: {
            // Import statements starting with "common" will be aliased to the common theme
            common: path.resolve(__dirname, '..', settings.commonTheme.srcPath),
        }
    },
    // Reduce + spread operator to combines all theme entries into a single object
    entry: settings.themes.reduce((obj, theme) => {

        // set up component override system
        let componentsMap = [
            // Commmon theme first
            ...glob.sync(`${settings.commonTheme.srcPath}/components/**/*.js*`),
            // Then current theme so it can override for the same component
            ...glob.sync(`${theme.srcPath}/components/**/*.js*`),
            // Same thing for elements
            ...glob.sync(`${settings.commonTheme.srcPath}/elements/**/*.js*`),
            ...glob.sync(`${theme.srcPath}/elements/**/*.js*`)
        ]
        // Exclude stories, those are only needed for storybook
            .filter(x => x.indexOf('.stories.js') === -1)
            // Exclude data, it is referenced by handlebar templates
            .filter(x => x.indexOf('.data.js') === -1)
            // Exclude data, it is referenced by handlebar templates
            .filter(x => x.indexOf('.knobs.js') === -1)
            .reduce((curr, componentPath) => {
                // Get the part after components, e.g. "callout/callout.js" to use as object key
                let componentName = componentPath.split('/components/')[1];
                // Since objects component will be added, or replaced with theme version if it already exists
                return ({ ...curr, [componentName]: componentPath });
            }, {});

        // We have an object, but we really want an array of file paths
        let components = Object.keys(componentsMap).map(x => componentsMap[x]);

        return {
            // Start with previous config
            ...obj,
            // Theme specific vendor.js
            [`${theme.key}|vendor`]: `${theme.srcPath}/javascripts/vendor.js`,
            // Theme specific main.js
            [`${theme.key}|main`]: [
                // Required for dynamic import
                'core-js/modules/es.promise',
                'core-js/modules/es.array.iterator',

                // Pull in images and fonts as is.  They'll be extracted to separate files
                ...glob.sync(theme.srcPath + '/images/**/*.*'),
                ...glob.sync(theme.srcPath + '/fonts/**/*.*'),

                // Pull in css.  It'll be extracted to a separate file
                `${theme.srcPath}/stylesheets/main.scss`, // Main CSS

                // Pull in main JS
                `${theme.srcPath}/javascripts/main.js`, // Global JS

                // Also include all component JS using overide system above
                ...components
            ],
        };
    }, {}),
    output: {
        publicPath: settings.publicPath,
        filename: (chunkData) => {
            const [themeName, fileName] = chunkData.chunk.name.split('|');
            return `${themeName}/js/${fileName}.js`;
        },
        chunkFilename: '[name].js',
        sourceMapFilename: '[file].map'
    },
    module: {
        rules: [
            // Create rules for handlebar templates, separated by theme
            ...settings.themes.map(theme => {
                return {
                    test: /\.(html|hbs|handlebars)$/,
                    exclude: /(node_modules)/,
                    // Only apply to files within theme
                    include: [path.resolve(theme.srcPath)],
                    use: [{
                        loader: 'handlebars-loader',
                        query: {
                            // This allows us to call data files from handlebar helpers
                            // Specifically we are limiting to files ending in
                            // `.knobs.js` and `.data.json` so other files don't get converted to require statements
                            inlineRequires: /\.(data.json|knobs.js)$/,
                            partialDirs: [
                                path.resolve(theme.srcPath), // Search in current theme's path
                                path.resolve(settings.commonTheme.srcPath) // and common theme path
                            ],
                            helperDirs: [
                                path.resolve(theme.srcPath, 'html/helpers'), // Search in current theme's path
                                path.resolve(settings.commonTheme.srcPath, 'html/helpers') // and common theme path
                            ],
                            extensions: ['.handlebars', '.hbs', '.html'],
                            precompileOptions: {
                                knownHelpersOnly: false,
                            },
                        }
                    }, {
                        // html-loader also converts the HTML to JS so we want to undo that part for the
                        // Handlebars loader
                        loader: 'extract-loader'
                    }, {
                        // Parse src and data-src attributes for images and rewrites the paths
                        // to the correct values
                        loader: 'html-loader',
                        options: {
                            attrs: ['img:src', 'img:data-src']
                        }
                    }]
                };
            }),

            // Run ESLint
            {
                test: /\.(js)$/,
                include: [path.resolve('../src')],
                exclude: [/node_modules/],
                enforce: 'pre',
                loader: 'eslint-loader',
                options: {
                    emitWarning: true,
                    // Autofix certain issues
                    fix: true,
                }
            },
            // Use babel for JS files
            {
                test: /\.js?$/,
                exclude: [/node_modules/],
                loader: 'babel-loader'
            },
            // Extract static assets to separate file
            ...settings.themes.map(
                theme => ({
                    test: /\.(gif|png|jpe?g|ico|woff|woff(2)?|ttf|eot|svg|otf)$/,
                    exclude: /(node_modules|svg-to-font)/,
                    // Only apply to files within theme
                    include: [path.resolve(theme.srcPath)],
                    use: {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            outputPath: theme.outputName,
                            publicPath: path.posix.join(settings.publicPath, theme.outputName),
                            context: path.resolve(theme.srcPath),
                        },
                    }
                })
            ),
            // Don't export mock data in production
            ...settings.isProduction ? [] : settings.themes.map(
                theme => ({
                    type: 'javascript/auto',
                    test: /\.(mock-data.json)$/,
                    // Only apply to files within theme
                    include: [path.resolve(theme.srcPath)],
                    use: {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            outputPath: theme.outputName,
                            publicPath: path.posix.join(settings.publicPath, theme.outputName),
                            context: path.resolve(theme.srcPath),
                        },
                    }
                })
            ),
            {
                test: /\.s?css$/i,
                exclude: /(node_modules)/,
                use: [
                    // NOTE: Configs diverge here.  Storybook uses the 'style-loader' to bring it inline
                    // The Dev/Prod builds use the MiniCssExtractPlugin to pull it into separate files
                    // Those are defined in the Storybook webpack, and in the Dev/Prod webpack configs

                    // translates CSS into CommonJS modules
                    { loader: 'css-loader', options: { sourceMap: true, importLoaders: 3, import: false, url: true } },

                    // Run post css actions.
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            plugins: (loader) => [
                                autoprefixer(),
                                new IconfontWebpackPlugin(loader),
                                cssnano({
                                    // Leave whitespace in dev build.
                                    preset: ['default', { normalizeWhitespace: settings.isProduction }]
                                })
                            ]
                        }
                    },
                    { loader: 'resolve-url-loader' },
                    {
                        loader: 'sass-loader',
                        // Allow using wildcard imports in scss
                        options: { sourceMap: true, sassOptions: { importer: sassGlobImporter() }, }
                    }
                ]
            },
        ]
    },
    plugins: [
        // Expose jQuery to the global scope
        // Prefer to import jQuery explicitly as it can guarantee no-conflict is applied
        // This exists for any external JS that might need jQuery
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            // Some jQuery plugins look for jQuery on the window object, provide it.
            'window.$': 'jquery',
            'window.jQuery': 'jquery',
        }),
        new MiniCssExtractPlugin({
            // In MiniCssExtractPlugin we use `moduleFilename` instead of filename to specify a function
            moduleFilename: (chunk) => {
                // names are specified as "theme|name"
                const [theme, name] = chunk.name.split('|');

                return `${theme}/css/${name}.css`;
            },
            chunkFilename: '[name].css',
            publicPath: settings.publicPath,
            ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
        // Create a HtmlWebpackPlugin for each html page in each theme
        // Ignore when in production
        ...settings.isProduction ? [] : settings.themes.reduce((currentArray, theme) => {
            // Paths to all html pages and subpages in a theme
            let paths = [
                ...glob.sync(theme.srcPath + '/html/*.hbs'),
                ...glob.sync(theme.srcPath + '/html/subpage/*.hbs')
            ];
            // Map it to an HtmlWebpackPlugin
            let htmlWebpackPlugins = paths.map(filePath =>
                new HtmlWebpackPlugin({

                    filename: filePath.replace(theme.srcPath, theme.outputName).replace('/html/', '/').replace('.hbs', '.html'),
                    // Template file
                    template: filePath,
                    inject: false,
                }));
            // Append to the previous results
            return [...currentArray, ...htmlWebpackPlugins];
        }, []),

    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                // Shared bundle for vendor code.  Seems like it has to be done this way for WebPack to not load scripts twice
                shared: {
                    chunks: 'initial',
                    test: /([\\/]node_modules[\\/])/,
                    name: 'shared'
                },
            }
        }
    },
};
export default config;