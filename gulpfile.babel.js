import gulp from 'gulp';

import checkVersionTask from './tasks/check-version';
import browserSyncTask from './tasks/browser-sync';
import webpackTask from './tasks/webpack';
import cleanTask from './tasks/clean';
import copyTasks from './tasks/copy-files';
import checkProdEnv from './tasks/check-prod-env';

export const clean = gulp.series(checkVersionTask, cleanTask);

export const dev = gulp.series(clean, gulp.parallel(browserSyncTask, webpackTask));

export const prod = gulp.series(checkProdEnv, clean, webpackTask, copyTasks);

export default dev;