# Magic Files and Folders

A "Magic" file or folder is referenced by some part of the build process explicitly
and as such should not be moved or renamed without understanding how they work.

A "Semi-Magic" folder may have Magic files inside, and so the folder should not be renamed
without updating the reference.

A "Semi-Magic" file may have a file extension that's treated differently, however the
name and location can be changed.

A "Convention Folder" is a normal folder, that is named/located by convention.  If it 
makes sense to rename or move it for your project, it can be done without changes
to the build configuration (code that references will obviously need to be updated). 

## Magic Folder: `.storybook` 
The storybook folder contains the configuretion for Storybook.  
It is split up further per theme, with some helper files for sharing code between them.  
For the files within each theme folder, refer to the Storybook documentation

Each theme runs it's own instance of Storybook,
and the `scripts` section of the `package.json` file has commands that point to the specific
folder for the config. 

 ## Magic Folder: `src/[theme]`
 These theme folders are the root of your source.  
 These themes and their paths are defined in `config/settings.js`
 
 ### Common theme
 One theme should be specified as the common theme (default to `src/_common`)
 
 This theme is special in that other themes will inherit from it.  
 See main README.md for details
 
## Magic Folder: `src/[theme]/components`
This folder is where components are stored.  
Sub folders can be created and nested arbitrarily deep to organize components.
  
All JS in components folder is automatically bundled into the final `main.js` file.
The JS bundling is configured in the webpack configuration with wildcards.

The `.scss` files are also automatically bundled, but this is done in
`src/[theme]/components/stylesheets/main.scss`

The handlebars templates are included by either from the root html files, 
or from stories depending on the context.

## Convention Folder:  `src/[theme]/components/[component path]/_story`
This is actually not a magic folder and is merely a convention.  
**However** the files within have "magic extensions".

### Magic File: `[...]/_story/[component name].stories.js`
The suffix `.stories.js` means that it will be executed by Storybook to look for stories.  
This is configured in `.storybook/[theme]/config.js`.  
Furthermore, `.stories.js` are excluded from the base webpack, 
and will not be included when running outside of Storybook.

### Semi-Magic File: `[...]/_story/[component name].data.json`
The suffix `.data.json` is meant to be used as default data passed to a handlebars template.
  
These are referenced in the handlebars templates as follows:
`{{#default-data "./_story/[component name].data.json"}}` 
Furthermore, `.data.json` are excluded from the base webpack, 
and will not be included when running outside of Storybook.

### Semi-Magic File: `[...]/_story/[component name].knobs.js`
The suffix `.knobs.js` is meant to be used as default data passed to a handlebars template.
It is used when the storybook knobs should be defined in the component rather than
in the story itself.  It is recommended, but not required, to import the initial data
for from `./[component name].data.json` so that can be used as a reference for the data format.
  
These are referenced in the handlebars templates as follows:  
`{{#default-data "./_story/[component name].knobs.js" "[optional tab path segment name]"}}`  
This is referenced **instead** of `.data.json`.
 
Furthermore, `.knobs.js` are excluded from the base webpack, 
and will not be included when running outside of Storybook.

## Magic Folder: `src/[theme]/elements`
Elements are functionally identical to components.  The difference is intent.   
Elements are not meant to be used standalone and are typically included in  
other components.

## Convention Folder:  `src/[theme]/fonts`
The `fonts` folder is actually **not** a magic folder.  The inclusion of fonts is based off
the file extension.

This is configured in `config/webpack.base.config.js`.  Look for the following comment:  
`// Extract static assets to separate file`  

There is a regex that configures which extensions are included as static assets. 
If additional extensions are required, that is where to add them.

## Magic Folder: `src/[theme]/html`
When running in gulp via `npm run dev`, handlebar templates in the ***root*** of this folder,
or in the `subpage` folder are treated as pages to be rendered.  
All components/placeholder/scaffold references will be called with the "default data" 
specified on each component, where applicable.

`.stories.js` files are treated the same way they are elsewhere.

### Magic Folder: `src/[theme]/html/helpers`
This is a magic folder, used by the webpack handlebars loader.  Handlebar helpers can be
defined here.  Unfortunately the loader has some limitations, so the file name is treated 
as the name of the helper, and helpers can only live in the root of this folder.  Subfolders are ignored.

### Convention Folder: `src/[theme]/html/layouts`
Not a magic folder, but layout files should be defined here.  
Layout files should contain the html/head/body tags.

### Convention Folder: `src/[theme]/html/placeholders`
Also not a magic folder. Placeholder templates are placed here.  
They are similar to components in usage, with some helper files and template
to help them dynamically render components.  

Currently they are not organized into subfolders, but if your project has many of them
feel free to put them into sub folders.
  
### Convention Folder: `src/[theme]/html/scaffolds`
Once again, not a magic folder, but this should contain your scaffolds which are your
various Bootstrap grids and general layout patterns.  
They should reference placeholders which define what components are allowed in the area.

### Magic Folder: `src/[theme]/html/subpage`
Pages in here are treated the same as in the html root.  This exists to confirm that
paths to static assets work correctly even when in a subpage. 

## Convention Folder: `src/[theme]/images`
Not a magic folder, but static assets go here.  They are defined by file extension.  
See the `fonts` folder info for details.  Feel free to rename or reorganize this folder.

## Semi-Magic Folder: `src/[theme]/javascripts`
There are 2 magic files in here, detailed below.  Any other files in here are only
called when referenced directly.  Utilities and helper JS files that can be reused across
components can go here.

### Magic File: `main.js`
`main.js` should include or reference any JS that is not related to a components or element.  
Ideally there shouldn't be too much here.

The default implementation calls `$(document).trigger('reinitialize');` which is
a convention that is meant to be used in place of a document ready event in components.  

The primary driver is that storybook doesn't reload the iframe, just changes the DOM, so 
when a different story is selected, JS code doesn't execute again.  

This can also be called in cases where a chunk of HTML is loaded into the DOM, either from
a template, or from an AJAX call.  This will ensure any components added within that HTML chunk
get initialized.
 
 ### Magic File: `vendor.js`  
 `vendor.js` will be executed in the `<head>` rather than the end of the `<body>`.  
 
 A typical use case is if jQuery needs to be exposed to 3rd party JS code.  
 If there is no 3rd party JS code, this file should be empty.
 
 ## Convention Folder: `src/[theme]/styleguide`  
 This should contain story/stories for a styleguide for the site/theme.  
 Functionally works the same as components.
 
 ## Magic File: `src/[theme]/stylesheets/main.scss`  
 This file is the entry point for all CSS for a theme.
 
 CSS from node_modules should be prefixed with `~` e.g.  
 `@import "~bootstrap/scss/bootstrap-grid";` or  
 `@import "~bootstrap";`  
 Note there cannot be a slash after `~`.
 
 This file should reference all scss files needed.
 
 
 ## Magic Folder: `src/[theme]/svg-to-font`   
 This folder should contain svg files meant to be converted to a font.  
 The svg files should be referenced in css via the custom attribute `font-icon`.  
 E.g.   
 `font-icon: url("../../../svg-to-font/alert.svg");`
 
 All referenced svgs will be combined into a single font that is encoded in the CSS file.
 
 The files in this folder are **excluded** from being output as static files. 
 