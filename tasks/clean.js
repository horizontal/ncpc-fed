import rimraf from 'rimraf';

import settings from '../config/settings';

function cleanTask(callback) {
    // Delete all files from the destination
    rimraf(settings.dest, callback);
}

export default cleanTask;