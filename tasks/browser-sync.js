
import BrowserSync from 'browser-sync';

import settings from '../config/settings';

import path from 'path';

const browser = BrowserSync.create();

export default function browserSyncTask() {

    let config = {
        // Folder to serve from
        server: settings.dest,
        // Whether to open default browser
        open: false,
    };

    // Start up browsersync
    browser.init(config);

    // Watch all files in public.  For some reason watching public/dev didn't work.
    browser.watch('public/**/*', function (event, file) {
        console.log(event, ' event on ', file);
        if (event === 'change') {
            // Reload all files of that type, there may be dependencies
            browser.reload('*' + path.parse(file).ext);
        }
    });
}