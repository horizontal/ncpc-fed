/**
 * Array.forEach does not support async, this does.
 * @param array
 * @param callback
 * @returns {Promise<void>}
 */
export const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

/**
 * No-Op.  Does nothing.  Use for when you need an empty function
 */
export const noop = () => {
    // do nothing
};