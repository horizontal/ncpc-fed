import settings from '../config/settings';
import color from 'gulp-color';

function checkProdEnv(done) {
    if (!settings.isProduction) {
        console.error(color('Incorrect Environment: Use `npm run build` instead of `gulp prod` to ensure correct environment variable ', 'RED'));
        process.exit(1);
    }
    done();
}


export default checkProdEnv;