import gulp from 'gulp';
import webpack from 'webpack-stream';

import settings from '../config/settings';

import devConfig from '../config/webpack.dev.config';
import prodConfig from '../config/webpack.prod.config';

const webpackConfig = settings.isProduction ? prodConfig : devConfig;

function webpackTask() {
    // We're ignoring the src file because the entry point is defined in the webpack config
    return gulp.src('not-a-real-file', { allowEmpty: true })
        .pipe(webpack(webpackConfig))
        .pipe(gulp.dest(settings.dest));
}

export default webpackTask;