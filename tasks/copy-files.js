import settings from '../config/settings';

import path from 'path';
import fileSystem from 'fs';
import gulp from 'gulp';
import rimraf from 'rimraf';

import {promisify} from 'util';

import {asyncForEach, noop} from './utils';


// Convert to promise so we can `await` it
const rimrafAsync = promisify(rimraf);


async function copyTask(done) {
    const tasks = [];

    await asyncForEach(settings.themes, async theme => {
        // `fileSystem.exists` is deprecated, using stat to check if file exists
        const themeCopyPathExists = await fileSystem.promises.stat(theme.copyPath).catch(noop);

        if (themeCopyPathExists) {

            if (theme.deleteCopyPath) {
                await deleteCopyPath(theme).catch(console.error);
            }

            const themeTask = () => {
                // Copy files from theme to copy path
                return gulp.src(path.resolve(settings.dest, theme.outputName, '**/*.*'))
                    .pipe(gulp.dest(path.resolve(theme.copyPath, theme.outputName)));
            };

            // Give it a display name for console output
            themeTask.displayName = `copyThemeFiles-${theme.outputName}`;

            tasks.push(themeTask);
        }
    });

    // `fileSystem.exists` is deprecated, using stat to check if file exists
    const commonThemeCopyPathExists = await fileSystem.promises.stat(settings.commonTheme.copyPath).catch(noop);

    if (commonThemeCopyPathExists) {
        // Copy files shared files to copy common path *without* theme name
        // Note: We don't clear the folder because it would have been cleared previously
        const commonThemeTask = () => gulp.src(path.resolve(settings.dest, '*.*'))
            .pipe(gulp.dest(path.resolve(settings.commonTheme.copyPath)));

        // Give it a display name for console output
        commonThemeTask.displayName = 'copySharedFiles';

        tasks.push(commonThemeTask);
    }

    return gulp.parallel(...tasks, (allDone) => {
        // Parallel tasks done
        allDone();
        // Copy task done
        done();
    })(); // gulp.parallel (or gulp.series) returns a function.  When called within a task, we need to execute that function
}


/**
 * Clears out the copy path.
 * There may be a .gitignore file there to ensure the folder exists.
 * If there is, re-create that file
 * @param theme
 * @returns {Promise<void>}
 */
async function deleteCopyPath(theme) {
    // Clear out the folder

    const gitIgnorePath = path.join(theme.copyPath, '.gitignore');

    // Try to read the file.  Ignore error if it doesn't exist
    let gitIgnoreFile = await fileSystem.promises.readFile(gitIgnorePath).catch(noop);

    // Delete
    await rimrafAsync(theme.copyPath);

    if (gitIgnoreFile) {
        await fileSystem.promises.mkdir(theme.copyPath);
        await fileSystem.promises.writeFile(gitIgnorePath, gitIgnoreFile);
    }
}

export default copyTask;