import semver from 'semver';
import { engines } from '../package';
import color from 'gulp-color';

function checkVersion(done) {
    const version = engines.node;
    if (!semver.satisfies(process.version, version)) {
        console.error(color(`NODE VERSION ERROR: Required node version ${version} not satisfied with current version ${process.version}.`, 'RED'));
        console.error(color(`Use NVM to install and/or use correct version of Node`, 'RED'));
        process.exit(1);
    }
    done();
}

export default checkVersion;