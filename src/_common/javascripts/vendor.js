// Any 3rd party libraries that need to be run globally go here
// Examples are bootstrap JS, or jQuery plugins.

// Uncomment below to expose jQuery to external JS not part of this bundle.
// For the purposes of things like jQuery plugins, we are already exposing
// jQuery to them via the Webpack ProvidePlugin, so no need to do so explicitly
import jQuery from 'jquery';
window.$ = jQuery;
