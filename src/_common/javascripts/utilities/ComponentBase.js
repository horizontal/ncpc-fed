
import $ from 'jquery';


export default class ComponentBase {
    /**
     * 
     * @param {string} rootCssClass The css class for the root element of this component
     */
    constructor(rootCssClass) {
        this.rootCssClass = rootCssClass;
    }

    /**
     * Initializes this component.  This should generally always be called unless unit testing specific functions.
     */
    initialize() {
        // This event is fired on page load, as well as when the page changes in Storybook (which resets the DOM but not reload the page).  
        // We can also manually fire it if we load new HTML from the server and need to re-run this code
        $(window).on('reinitialize', () => {
            this._init();
        });
    }

    /**
     * This is called for each instance of a component
     * @param {jQuery} _$componentInstance 
     */
    onInitElement(_$componentInstance) {

    }

    /**
     * This is called with a jQuery selector with all instances of this component
     * @param {jQuery} $elements 
     */
    onInit($elements) {
        $elements.each((_index, element) => {

            const $element = $(element);

            $element.addClass(`${this.rootCssClass}-initialized`);

            this.onInitElement($element);
        });
    }

    _init() {
        // Only grab elements that have not yet been initialized
        const $elements = $(`.${this.rootCssClass}:not(.${this.rootCssClass}-initialized)`);

        // First check if our component is even on the page
        if (!$elements.length) {
            // If it's not, exit early
            return;
        }

        this.onInit($elements);
    }
}