import $ from 'jquery';

export function listenForStorybookPageChange(callback) {
    // Storybook updates an element #root.  
    let root = $('#root')[0];

    // If it doesn't exist, return
    if (!root) {
        return;
    }

    // setup mutation observer
    const observer = new MutationObserver((mutationsList) => {
        for (let i = 0, len = mutationsList.length; i < len; i++) {
            if (mutationsList[i].type === 'childList') {
                callback();
                break;
            }
        }
    });

    observer.observe(root, { childList: true, subtree: false });
}