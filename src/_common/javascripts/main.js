// global JS

// Don't include JS from components, they will be included automatically by WebPack

import $ from 'jquery';
import {listenForStorybookPageChange} from './utilities/storybook-helper';

$(() => {
  // Trigger reinitialize on document load
  $(document).trigger('reinitialize');

  // Also call when storybook changes the page
  listenForStorybookPageChange(() => {
    $(document).trigger('reinitialize');
  });

  var $navbarToggle = $('#mobilemenu'),
      clickstate;

  $navbarToggle.click(function() {
    if (clickstate !== 1) {
      $('#navbarCollapse').addClass('nav-expand');
      $(this).removeClass('collapsed').attr('aria-expanded', true);
      $('body').addClass('nav-expand');
      clickstate = 1;
    } else {
      $('#navbarCollapse').removeClass('nav-expand');
      $(this).addClass('collapsed');
      $(this).attr('aria-expanded', false);
      $('body').removeClass('nav-expand');
      clickstate = 0;
    }
    console.log('Mobile Menu state: ' + clickstate);
  });

  $(window).on('resize', function(){
    var win = $(this); //this = window
    if (win.width() >= 992) {
      $('#navbarCollapse').removeClass('nav-expand');
      $('#mobilemenu').addClass('collapsed').attr('aria-expanded', false);
      $('body').removeClass('nav-expand');
      clickstate = 0;
    }
  });
});
