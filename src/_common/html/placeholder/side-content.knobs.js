import { getPlaceholderData } from './helpers/placeholder-utils';

const allowedComponents = { 'Dropdown': 'dropdown'};

const defaultComponents = ['dropdown'];

export default (placeholderArray) => {    
    return getPlaceholderData(allowedComponents, defaultComponents, placeholderArray);
};