import { optionsKnob as options, boolean } from '@storybook/addon-knobs';


export const getGroupId = function(placeholderArray){
    return `${placeholderArray.length ? placeholderArray.join('>') : 'Root'}`;
};

export const getPlaceholderData = function (allowedComponents, defaultComponents, placeholderArray, componentName) {
    const groupId = getGroupId(placeholderArray);
    return {
        components: options(componentName || 'Components',
            { ...allowedComponents }, // Shallow copy into new object
            [ ...defaultComponents ], // Shallow copy into new array
            { display: 'multi-select' },
            groupId),

        highlight: boolean('Highlight', false, groupId)
    };
};