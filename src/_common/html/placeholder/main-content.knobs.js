import { getPlaceholderData } from './helpers/placeholder-utils';

const allowedComponents = {
    'Callout': 'callout',
    'Dropdown': 'dropdown',
    'Grid': 'grid',
    'Tabbed Container': 'tabs',
};
const defaultComponents = ['callout', 'dropdown'];

export default (placeholderArray) => {    
    return getPlaceholderData(allowedComponents, defaultComponents, placeholderArray);
};
