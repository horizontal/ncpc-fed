import { getPlaceholderData } from './helpers/placeholder-utils';

const allowedComponents = {
    'Dropdown': 'dropdown',
    'Callout': 'callout',
};

const defaultComponents = ['dropdown'];

export default (placeholderArray) => {
    return getPlaceholderData(allowedComponents, defaultComponents, placeholderArray);
};