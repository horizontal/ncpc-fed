import { getPlaceholderData } from './helpers/placeholder-utils';

const allowedComponents = {
    'Full Width Grid': 'full-width',
    'Half Width Grid': 'half-width',
    'One Thirds | Two Third Grid': 'four-eight',
    'Two Thirds | One Third Grid': 'eight-four',
};
const defaultComponents = ['eight-four'];

export default (placeholderArray) => {
    return getPlaceholderData(allowedComponents, defaultComponents, placeholderArray, 'Grid');
};