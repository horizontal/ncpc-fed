export default { title: 'Pages|Home' };

import html from './home.hbs';

export const withDefault = () => html();

export const withData = () => html({
  mainHeroText: {
    'showImage': true,
    'title': 'No-Code Preference Center',
    'subtitle': 'The first fully configurable preference center for Marketing Cloud',
    'cta': 'Get a DEMO',
    'url': '#'
  }
});
