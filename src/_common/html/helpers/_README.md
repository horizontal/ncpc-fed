# Handlebars HTML Helpers

Files put in this folder are automatically pulled in as Handlebars helpers by the Handlebars Webpack loader.

Refer to Handlesbars documentation for more details on helpers:
https://handlebars-draft.knappi.org/guide/expressions.html#helpers

https://handlebars-draft.knappi.org/guide/block-helpers.html#basic-blocks


A couple notes:
1. Sub folders do not work here, helpers need to be at the root of this folder
2. The file name becomes the name of the helper.  
3. Most of Handlebars functionality should work, but some advanced functions, like dynamic partial views, do not.
4. Themes should not have their own helpers, they should be put here in the common theme and shared.
