/**
 * `equals` Subexpression helper to check equality.
 * Use like: `{{#if (equal this 'full-width')}}` Don't forget the parentheses 
 * @param {*} first 
 * @param {*} second 
 */
export default function equal(first, second) {    
    return first === second;
}