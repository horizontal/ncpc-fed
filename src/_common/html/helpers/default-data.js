// Array of placeholders to allow nesting
let placeholderArray = [];

/**
 * Renders the contents using the default data form the specified file
 * @param data
 * @param optionsOrDataPlacehold
 * @param options
 * @returns {*}
 */
export default function (data, optionsOrDataPlacehold, options) {

    // We can provide a parameter to the data function
    let opts = options ? options : optionsOrDataPlacehold;

    // If a 3rd parameter is passed, the 2nd one will be our parameters
    let placeholder = options ? optionsOrDataPlacehold : undefined;

    // If there is a placeholder specified, add it to the array
    if (placeholder) {
        placeholderArray.push(placeholder);
    }

    let result;
    
    // If data is passed in explicitly, don't use the default data passed in
    // Unless this is coming from webpack, in which case, disregard
    if (opts.data.root && !opts.data.root.webpackConfig) {
        result = opts.fn(this);
    }
    else {
        // Depending on if it's from a JS export default or from a JSON, we may need to get the `.default` property
        let defaultData = data.default ? data.default : data;

        // Data could be added as a function that an array of placeholders to scope knobs
        if (typeof defaultData === 'function') {
            defaultData = defaultData(placeholderArray);
        }

        result = opts.fn(defaultData);
    }

    // If there is a placeholder specified, remove it from the array after rendering the results.
    // This needs to be called *after* `opts.fn()` because any inner renderings will need to have the current placeholders
    // After all components are rendered, the array should be empty again.
    if (placeholder) {
        placeholderArray.pop();
    }

    return result;
}