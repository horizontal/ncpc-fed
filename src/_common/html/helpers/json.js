/**
 * Renders object as JSON.  Use for debugging
 * @param {Object} data 
 * @returns {String}
 */
export default function (data) {    
    return JSON.stringify(data);
}