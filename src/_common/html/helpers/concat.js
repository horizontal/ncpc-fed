/**
 * `equals` Subexpression helper to check equality.
 * Use like: `{{#if (equal this 'full-width')}}` Don't forget the parentheses 
 * @param {*} current 
 * @param {*} next 
 */
export default function equal(current, next) {    
    return `${current} > ${next}`;
}