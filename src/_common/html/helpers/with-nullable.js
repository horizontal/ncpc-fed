import { isEmpty, isFunction }  from 'handlebars/lib/handlebars/utils';


export default function (context, options) {
    if (arguments.length != 2) { throw new Error('#with-nullable requires exactly one argument'); }
    if (isFunction(context)) { context = context.call(this); }

    let fn = options.fn;

    if (!isEmpty(context)) {
        let data = options.data;

        return fn(context, {
            data: data,
            blockParams: [context]
        });
    } else {
        return options.fn(this);
    }
}
