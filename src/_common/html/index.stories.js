export default { title: 'Pages|Index' };

import html from './index.hbs';

export const withOptions = () => html();

export const withPredefinedData = () => html({
    header: {
        title: 'Fixed Header'
    },
    body: {
        components: ['eight-four'],
        componentsData: {
            'eight-four': {
                'Right': {
                    components: ['dropdown']
                },
                'Left': {
                    components: ['callout', 'dropdown', 'grid'],

                    componentsData: {
                        'callout': {
                            showImage: false,
                            title: 'Super Products',
                            items: [
                                { title: 'Sun', description: 'Very bright' },
                                { title: 'Moon', description: 'Less bright' },
                            ]
                        },
                        'grid': {
                            components: ['eight-four'],
                            componentsData: {
                                'eight-four': {
                                    'Left': {
                                        components: ['callout'],
                                        componentsData: {
                                            'callout': {
                                                showImage: true,
                                                title: 'Inner Products',
                                                items: [
                                                    { title: 'Green' },
                                                ]
                                            }
                                        }
                                    },
                                    'Right': {
                                        components: ['dropdown']
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
});

withPredefinedData.story = {
    parameters: {
        knobs: { disabled: true } 
    }
};