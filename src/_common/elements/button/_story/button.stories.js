import { select } from '@storybook/addon-knobs';

import template from '../button.hbs';


import storybookCodePanel from 'storybook-code-panel';

export default {
    title: 'Elements|Simple/Button',
    parameters: {
        storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
    }
};

export const withOptions = () => {
    const options = {
        'Primary': 'btn-primary',
        'Secondary': 'btn-secondary',
        'Success': 'btn-success',
        'Danger': 'btn-danger',
        'Warning': 'btn-warning',
    };
    return template({
        style: select('Style', options, 'btn-primary')
    });
};

export const primary = () => template({ style: 'btn-primary' });
export const secondary = () => template({ style: 'btn-secondary' });
export const success = () => template({ style: 'btn-success' });
export const danger = () => template({ style: 'btn-danger' });
export const warning = () => template({ style: 'btn-warning' });
