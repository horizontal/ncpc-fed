import ComponentBase from 'common/javascripts/utilities/ComponentBase';

/**
 * A tabbed container component.  This JS exists to demonstrate using dynamic imports
 */
class TabbedContainer extends ComponentBase {

    constructor() {
        super('tabbed-container');
    }

    onInit($allElements) {

        // NOTE: This is an example of dynamic import.  This should be used very sparingly.  Use a dynamic import
        // for especially large JS libraries that is only needed for certain components, and will not be needed
        // on most pages.

        // Dynamically import bootstrap tab.  Because the component base only calls onInit if the component is on the page
        // the file will only be included when this component is on the page.

        // `webpackChunkName` is a special comment the indicates the name for the bundle.
        // If there are multiple imports that should be bundled together, give them the same name

        import(/* webpackChunkName: "bootstrap-tab" */ 'bootstrap/js/dist/tab')

            // The import returns a promise, so call code that depends on the import after it loads
            .then(() => {
                super.onInit($allElements);
            });
    }
}

// Create component
const tabbedContainer = new TabbedContainer();

// and initialize it
tabbedContainer.initialize();
