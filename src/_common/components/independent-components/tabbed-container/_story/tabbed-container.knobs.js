import { text, number } from '@storybook/addon-knobs';

import { getGroupId } from 'common/html/placeholder/helpers/placeholder-utils';

import data from './tabbed-container.data';

export default (placeholderArray) => {

    const groupId = getGroupId(placeholderArray);

    // Knob fields show up in the order that the functions are called
    // This is why the simple ones are defined up here instead of inl
    const title = text('Tab Container Title', data.title, groupId);
    const count = number('Number of tabs', data.tabs.length, {
        range: true,
        min: 0,
        max: 5,
        step: 1,
    }, groupId);
    const tabs = [];
    for (let i = 0; i < count; i++) {
        const tab = data.tabs.length > i ? data.tabs[i] : { title: 'New Tab', id: `tab-${i+1}` };
        
        const itemGroupId = `${groupId}>${tab.id}`;
        tabs.push({
            id: tab.id,
            title: text('Title', tab.title, itemGroupId),
            active: tab.active
        });
    }
    return {
        id: data.id,
        title: title,
        tabs: tabs
    };
};