import template from '../tabbed-container.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
    title: 'Components|Independent/Tabbed Container',
    parameters: {
        storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
    }
};

export const withOptions = () => template();
