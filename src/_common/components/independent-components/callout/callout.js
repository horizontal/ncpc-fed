import Callout from './CalloutClass';

// Create component
const callout = new Callout();

// and initialize it
callout.initialize();