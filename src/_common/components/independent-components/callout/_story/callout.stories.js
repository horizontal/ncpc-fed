import template from '../callout.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
    title: 'Components|Independent/Callout',
    parameters: {
        storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
    }
};

export const withOptions = () => template();

export const withoutImage = () => template({
    showImage: false,
    title: 'Featured Products',
    items:
        [
            { title: 'Item 1', description: 'First Item' },
            { title: 'Item 2', description: 'Second Item' },
        ]
});

withoutImage.story = {
    parameters: {
        knobs: { disabled: true } 
    }
};