import { boolean, text, number } from '@storybook/addon-knobs';

import { getGroupId } from 'common/html/placeholder/helpers/placeholder-utils';

import data from './callout.data';

export default (placeholderArray) => {

    const groupId = getGroupId(placeholderArray);

    // Knob fields show up in the order that the functions are called
    // This is why the simple ones are defined up here instead of inline
    const showImage = boolean('Show Image', data.showImage, groupId);
    const title = text('Title', data.title, groupId);
    const count = number('Number of items', data.items.length, {
        range: true,
        min: 0,
        max: 10,
        step: 1,
    }, groupId);
    const items = [];
    for (let i = 0; i < count; i++) {
        const item = data.items.length > i ? data.items[i] : { title: 'New Item' };
        items.push({
            title: text(`Item ${i + 1} > Title`, item.title, groupId),
            description: text(`Item ${i + 1} > Description`, item.description, groupId)
        });
    }
    return {
        showImage: showImage,
        title: title,
        items: items
    };
};