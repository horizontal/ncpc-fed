import { text } from '@storybook/addon-knobs';

import { getGroupId } from 'common/html/placeholder/helpers/placeholder-utils';

import data from './video.data';

export default (placeholderArray) => {

  const groupId = getGroupId(placeholderArray);

  // Knob fields show up in the order that the functions are called
  // This is why the simple ones are defined up here instead of inline
  const id = text('Video ID', data.id, groupId);
  const title = text('Video Player Title', data.title, groupId);
  const poster = text('Video Poster', data.poster, groupId);

  return {
    title: title,
    id: id,
    poster: poster
  };
};
