import VideoPlayer from './VideoPlayerClass';

// Create component
const videoplayer = new VideoPlayer();

window.$(document).ready(function() {

  window.$('.video-tile').on('click', function() {
    $('.play-video, .video-tile img').css('opacity', '0');

    var videoid = window.$(this).data('id'),
      player = window.$(this).data('video'),
      videoFile = '';

    if (player === 'youtube') {
      videoFile = 'https://www.youtube.com/embed/' + videoid + '?autoplay=1&rel=0';
    } else if (player === 'vimeo') {
      videoFile = 'https://player.vimeo.com/video/' + videoid + '?api=1&background=0&muted=0&loop=0&autoplay=1';
    }

    window
      .$(this)
      .find('.video-tile-loader')
      .css({
        display: 'block',
        opacity: 1
      })
      .attr('src', videoFile);
  });
});

// and initialize it
videoplayer.initialize();
