import ContentArea from './ContentAreaClass';

// Create component
const contentarea = new ContentArea();

// and initialize it
contentarea.initialize();
