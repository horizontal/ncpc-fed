import contentarea from '../contentarea.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
  title: 'Components|Independent/Content Area',
  parameters: {
    storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
  }
};

export const withOptions = () => contentarea();
