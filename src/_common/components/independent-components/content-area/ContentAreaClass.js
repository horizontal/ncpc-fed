
import ComponentBase from 'common/javascripts/utilities/ComponentBase';

// Class is in a separate file so that if we want to override in another theme
// we can import the base class without initialzing it

export default class contentarea extends ComponentBase {

  constructor() {
    super('contentarea');
  }

  onInitElement($contentarea) {
    // Prevent event handler for attaching more than once
    $contentarea
      .off('click', this.onClick)
      .on('click', this.onClick);
  }
}
