import showcase from '../showcase.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
  title: 'Components|Independent/Showcase',
  parameters: {
    storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
  }
};

export const withOptions = () => showcase();
