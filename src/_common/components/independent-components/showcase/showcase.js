import Showcase from './ShowcaseClass';

// Create component
const showcase = new Showcase();

// and initialize it
showcase.initialize();
