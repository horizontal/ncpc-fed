
import ComponentBase from 'common/javascripts/utilities/ComponentBase';

// Class is in a separate file so that if we want to override in another theme
// we can import the base class without initialzing it

export default class showcase extends ComponentBase {

  constructor() {
    super('showcase');
  }

  onInitElement($showcase) {
    // Prevent event handler for attaching more than once
    $showcase
      .off('click', this.onClick)
      .on('click', this.onClick);
  }
}
