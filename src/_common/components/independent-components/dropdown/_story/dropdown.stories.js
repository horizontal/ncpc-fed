import Readme from './dropdown.readme.md';


import storybookCodePanel from 'storybook-code-panel';

export default {
    title: 'Components|Independent/Dropdown',
    parameters: {
        storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/)),
        readme: {
            // Show readme at the addons panel
            sidebar: Readme,
        },
    }
};

import dropdown from '../dropdown.hbs';

export const withDefault = () => dropdown();
