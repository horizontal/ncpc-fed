# Dropdown Component

Mock Data can be included as a JSON file.  
The ajaxurl and ajaxmethod will be set differently on the back-end.

```html
<div class="dropdown" 
    data-ajaxurl="/common/components/independent-components/dropdown/dropdown.mock-data.json"
    data-ajaxmethod="GET">
```

Front-end static only supports GET for dummy json data, however backend can use whatever method is appropriate.