import ComponentBase from 'common/javascripts/utilities/ComponentBase';

// Import bootstrap's dropdown
// Instead of loading all of bootstrap's JS, we are loading individual components that we need
import 'bootstrap/js/dist/dropdown';

import $ from 'jquery';

// We can use our handlebar templates from the client-side as well.  It will be compiled into the JS.
import dropdownMenuTemplate from './dropdown-menu-template.hbs';

export default class DropDown extends ComponentBase {

    constructor() {
        super('dropdown');
    }

    onInitElement($dropdown) {
        const url = $dropdown.data('ajaxurl');
        const method = $dropdown.data('ajaxmethod');
        $.ajax({
            method: method,
            url: url
        }).then(data => {
            let html = dropdownMenuTemplate(data);
            $dropdown.find('.dropdown-menu').replaceWith(html);

            // Prevent event handler for attaching more than once
            $dropdown
                .off('click', '.dropdown-item', this.onDropdownItemClick)
                .on('click', '.dropdown-item', this.onDropdownItemClick);
        });
    }

    onDropdownItemClick(e) {
        e.preventDefault();
        return alert('jquery!');
    }
}

// Create component
const dropdown = new DropDown();

// and initialize it
dropdown.initialize();
