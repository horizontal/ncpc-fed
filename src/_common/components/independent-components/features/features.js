import Features from './FeaturesClass';

// Create component
const features = new Features();

// and initialize it
features.initialize();
