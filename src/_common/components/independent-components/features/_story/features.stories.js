import features from '../features.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
  title: 'Components|Independent/Features',
  parameters: {
    storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
  }
};

export const withOptions = () => features();
