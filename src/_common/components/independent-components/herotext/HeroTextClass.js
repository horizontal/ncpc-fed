
import ComponentBase from 'common/javascripts/utilities/ComponentBase';

// Class is in a separate file so that if we want to override in another theme
// we can import the base class without initialzing it

export default class HeroText extends ComponentBase {

    constructor() {
        super('herotext');
    }

    onInitElement($herotext) {
        // Prevent event handler for attaching more than once
        $herotext
            .off('click', this.onClick)
            .on('click', this.onClick);
    }

    onClick(e) {
        e.preventDefault();
        return alert('Common theme!');
    }
}
