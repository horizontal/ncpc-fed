import HeroText from './HeroTextClass';

// Create component
const herotext = new HeroText();

// and initialize it
herotext.initialize();
