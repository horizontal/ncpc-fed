import { boolean, text, number } from '@storybook/addon-knobs';

import { getGroupId } from 'common/html/placeholder/helpers/placeholder-utils';

import data from './herotext.data';

export default (placeholderArray) => {

    const groupId = getGroupId(placeholderArray);

    // Knob fields show up in the order that the functions are called
    // This is why the simple ones are defined up here instead of inline
    // const showImage = boolean('Show Image', data.showImage, groupId);
    const title = text('Title', data.title, groupId);
    const subtitle = text('Sub Title', data.subtitle, groupId);
    const cta = text('CTA', data.cta, groupId);
    const url = text('URL', data.url, groupId);

    return {
        title: title,
        subtitle: subtitle,
        cta: cta,
        url: url
    };
};
