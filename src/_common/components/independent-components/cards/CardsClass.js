
import ComponentBase from 'common/javascripts/utilities/ComponentBase';

// Class is in a separate file so that if we want to override in another theme
// we can import the base class without initialzing it

export default class cards extends ComponentBase {

  constructor() {
    super('cards');
  }

  onInitElement($cards) {
    // Prevent event handler for attaching more than once
    $cards
      .off('click', this.onClick)
      .on('click', this.onClick);
  }
}
