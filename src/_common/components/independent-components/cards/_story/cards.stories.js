import cards from "../cards.hbs";

import storybookCodePanel from 'storybook-code-panel';

export default {
  title: 'Components|Independent/Cards',
  parameters: {
    storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
  }
};

export const withOptions = () => cards();
