import TEMPLATE from './TEMPLATEClass';

// Create component
const template = new TEMPLATE();

// and initialize it
template.initialize();
