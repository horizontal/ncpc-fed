import contactform from '../contactform.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
  title: 'Components|Independent/Contact Form',
  parameters: {
    storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
  }
};

export const withOptions = () => contactform();
