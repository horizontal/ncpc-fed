import ContactForm from './ContactFormClass';

// Create component
const contactform = new ContactForm();

window.$(document).ready(function() {
  window.$('.form p').addClass('form-group');

  let input = window.$('.form p.form-field').find('input');

  input.each(function() {
    let $this = window.$(this),
        $label = window.$($this).prev();

    window.$($this).attr('required', ''); // Add the required attribute
    window.$($this).insertBefore(window.$($label)); // Swap input and label positions (label must follow input)
  });
});

// and initialize it
contactform.initialize();
