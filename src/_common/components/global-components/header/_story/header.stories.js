import {text, boolean} from '@storybook/addon-knobs';

import template from '../header.hbs';

import data from './header.data';

import storybookCodePanel from 'storybook-code-panel';

export default {
  title: 'Components|Global/Header',
  parameters: {
    storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
  }
};

export const withOptions = () => {
  return template({
    title: text('Title', data.title),
    hideImage: boolean('Hide Image', data.hideImage)
  });
};
