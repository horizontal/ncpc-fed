import {text} from '@storybook/addon-knobs';

import template from '../footer.hbs';

import data from './footer.data';

import storybookCodePanel from 'storybook-code-panel';

export default {
    title: 'Components|Global/Footer',
    parameters: {
        storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!../', false, /^((?!stories).)*$/))
    }
};

export const withOptions = () => template({
    footerText: text('Footer Text', data.footerText)
});

