import template from './styleguide.hbs';

import storybookCodePanel from 'storybook-code-panel';

export default {
    title: 'Styleguide|Styles',
    parameters: {
        storybookCodePanel: storybookCodePanel.createParams(require.context('!!raw-loader!./', false, /^((?!stories).)*$/))
    }
};



const data = {
    colors: [{
        name: 'Black', hex: '#2F2D2E', class: 'styleguide-color_black', fullWidth: true
    }, {
        name: 'Dark Gray', hex: '#515152', class: 'styleguide-color_dark-gray'
    }, {
        name: 'Medium Gray', hex: '#787779', class: 'styleguide-color_medium-gray'
    }, {
        name: 'Light Gray', hex: '#CFCDC8', class: 'styleguide-color_light-gray'
    }, {
        name: 'Aqua', hex: '#7FC3BA', class: 'styleguide-color_aqua'
    }, {
        name: 'Salmon', hex: '#F58466', class: 'styleguide-color_salmon'
    }, {
        name: 'Pink', hex: '#F8AA97', class: 'styleguide-color_pink'
    }, {
        name: 'Blue', hex: '#065264', class: 'styleguide-color_blue'
    }, {
        name: 'Forest', hex: '#2E4E40', class: 'styleguide-color_forest'
    },]
};

export const vanilla = () => template(data);

export const bodyClassType1 = () => template(data);
bodyClassType1.story = {
    parameters: {
        bodyClass: 'type-1'
    }
};

export const bodyClassType2 = () => template(data);
bodyClassType2.story = {
    parameters: {
        bodyClass: 'type-2'
    }
};