SVG files put in here that are referenced via CSS will be converted to a font.  

The font is rendered inline in the CSS. It will not be in a separate file.

These fonts will *not* be copied to the output directory.