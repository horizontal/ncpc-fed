// This allows us to pass-through the common stories if we don't have anything to add

// Default value needs to be exported separately
export {default} from 'common/components/independent-components/callout/_story/callout.stories';

// Then export the invidual stories
export * from 'common/components/independent-components/callout/_story/callout.stories';