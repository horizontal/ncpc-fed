import CalloutBase from 'common/components/independent-components/callout/CalloutClass';

// Exactly the same as the base class, except we are override the `onClick` function

export default class Callout extends CalloutBase {

    onClick(e) {
        e.preventDefault();
        return alert('brand-1!');
    }
}