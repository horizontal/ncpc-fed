# Storybook in HIVE
Storybook will be used as a pattern library for new components.

It allows us to view individual components as "stories" and the ability to render them with different settings.

With the "Knobs" plugin, we are able to dynamically configure settings for components, allowing a light-weight content editing experience.

We also have included the concept of "placeholders" that allow dynamically placing components on a page.

## Getting Started
Ensure all prereqs are set up and `npm install` has been run (see README.md for details).

Running `npm run storybook-all` will open a separate storybook instance for each theme on a different port.  
When adding or removing themes be sure update the `scripts` section of `package.json` for the storybook tasks.

## Creating Components
Refer to `README.md` for information on theming.

Inside a theme, components go into `components` folder.

Within the `components` folder, components should be grouped into a folder structure that makes sense for the project.

Each component should have its own folder.

That folder should contain some or all of the following files:

* component-name (Folder)
    * _story (Folder)
        * component-name.stories.js (Required)
            * Stories describing for the component.  See below for more details.
        * component-name.data.js (Optional)
            * Default data for rendering a component.  See below for more details.
    * _component-name.scss (Optional)
        * Component specific css.  Styling should be scoped to a particular component
    * component-name.hbs (Required)
        * Handlebars template containing the markup for the component.  See below for more details.
    * component-name.js (Optional)
        * Component specific JavaScript.

Files under the `_story` folder are for Storybook, and will not be included in the output for back-end.  Note: the `_story` folder is merely a convention for clarity.  The story and data files however must end in `.stories.js` and `.data.js` (or `.data.json`) respectively.


### Simple Example

Let's look at a simple component

#### Handlebars template
`components/global-components/footer/footer.hbs`
```handlebars 
{{#default-data "./_story/footer.data.json"}}
    <footer class="footer">
        {{ footerText }}
    </footer>
{{/default-data}}
```

The first thing to note is the #default-data helper wrapping the whole component.  The string parameter is the relative path to the file that contains the default data for the component.

That data will be passed to the inner component to use.  In this case, the `footerText` is dynamic.

While using data is not strictly required, anything that is meant to be content managed should be defined with a data object to ensure compatibility with the back-end code.  This also applies to any settings or options a component may have.

#### Default Data
`components/global-components/footer/_story/footer.data.json`
```json
{
  "footerText": "Default Footer"
}
```
This file should define all the fields used in the component as well as their default values.
In a simple case, a `.data.json` file will suffice.

NOTE: This file *must* end in `.data.js` or `.data.json`.  
Ensure that the filename is correct in the `#default-data` helper in the `.hbs` template.

#### Stories

`components/global-components/footer/footer.stories.js`
```js
import {text} from '@storybook/addon-knobs';

import template from '../footer.hbs';

import data from './footer.data';

export default {title: 'Components|Global/Footer'};

export const withOptions = () => template({
    footerText: text('Footer Text', data.footerText)
});
```

There are two required pieces for a story file.  

First is the story definition 

`export default {title: 'Components|Global/Footer'};`

The first part of the title is the top level grouping separated with a `|`.  The rest indicate a folder structure separated by `/`.  This determines where these stories appear in the UI.

The next part represents the individual story itself:
```js
export const withOptions = () => template({
    footerText: text('Footer Text', data.footerText)
});
```
The name of the export `withOptions` gets converted to the display name as "With Options".

It should be a function which returns the rendered HTML as a string.

```js
import {text} from '@storybook/addon-knobs';

import template from '../footer.hbs';

import data from './footer.data';
```

The first import is from the Knobs addon which gives a UI for editing data on the fly.

The knob functions generally take a name, an initial value, and optional `groupId` which is the name of the tab to show the settings. Depending on the knob different parameters are also available. See https://github.com/storybookjs/storybook/tree/master/addons/knobs for more details.

The `template` import is a function which renders the imported handlebars template, passing in the data specified.

For more details on stories, see the storybook docs:

https://storybook.js.org/docs/basics/writing-stories/



### More Complex Example

Let's look at a more complex component

#### Handlebars template
`components/independent-components/callout/callout.hbs`
```handlebars 
{{#default-data "./_story/callout.data.js" "Callout"}}
    <div class="callout">
        {{#if showImage }}
            <div class="callout-background-image"></div>
        {{/if}}
        <div class="callout-title">{{title}}</div>
        <ul class="callout-content">
            {{#each items }}
                <li>
                    <a href="#">{{ this.title }}</a> {{#if this.description }} - {{ this.description }} {{/if}}
                </li>
            {{ else }}
                <li>No items</li>
            {{/each}}
        </ul>
    </div>
{{/default-data}}
```

As before we wrap the component with our #default-data helper.  This time, our data is a `.js` file not a `.json` file.  We are also passing an additional parameter with the name of the component.  This passed to the data file and used to generate the tab name in the knobs, as we'll see below.

Here you can see were are conditional rendering some element, and we are also looping through an array of items, each with its own fields.

Try it:
Play around with the component in the Storybook UI and see how this changes the component.

#### Default Data
`components/independent-components/callout/_story/callout.data.js`
```js
import { boolean, text, number } from '@storybook/addon-knobs';

import { getGroupId } from 'common/html/placeholder/helpers/placeholder-utils';

const data = {
    showImage: true,
    title: 'Featured Products',
    items:
        [
            { title: 'Item 1', description: 'First Item' },
            { title: 'Item 2', description: 'Second Item' },
            { title: 'The Time Is', description: new Date().toString() }
        ]
};

export default (placeholderArray) => {

    const groupId = getGroupId(placeholderArray);

    // Knob fields show up in the order that the functions are called
    // This is why the simple ones are defined up here instead of inline
    const showImage = boolean('Show Image', data.showImage, groupId);
    const title = text('Title', data.title, groupId);
    const count = number('Number of items', data.items.length, {
        range: true,
        min: 0,
        max: 10,
        step: 1,
    }, groupId);
    const items = [];
    for (let i = 0; i < count; i++) {
        const item = data.items.length > i ? data.items[i] : { title: 'New Item' };
        const itemGroupId = `${groupId}>Item ${i + 1}`;
        items.push({
            title: text('Title', item.title, itemGroupId),
            description: text('Description', item.description, itemGroupId)
        });
    }
    return {
        showImage: showImage,
        title: title,
        items: items
    };
};
```

There's quite a lot going on here.  First, this is a `.js` file because we have some custom logic.

Second, we are exporting a function which takes a `placeholderArray` parameter.  This parameter is passed to us by our `#default-data` helper so we can keep track of where we are with nested components.

Third is that were are defining the knobs in the data rather than at the story level.  This allows the knobs to show up even when this component is pulled in from a different component.  **Whether this is desired can vary from project to project, or even by component.  Choose the appropriate format for your project/component**

We define our `data` object above.  This is recommended for readability so we can see at a glance what the shape of the data looks like without all out custom logic.

Another thing of note is that since the `count` variable is a knob, the value can change dynamically, thus we can change the number of items dynamically.

In this case, we are putting each item in a different group, e.g. a new tab.  It is also possible to put them in the same tab, but prefix the name of the knob instead.

#### Stories

`components/independent-components/callout/_story/callout.stories.js`
```js
import template from '../callout.hbs';

export default {title: 'Components|Independent/callout'};

export const withOptions = () => template();

export const withoutImage = () => template({
    showImage: false,
    title: 'Featured Products',
    items:
        [
            { title: 'Item 1', description: 'First Item' },
            { title: 'Item 2', description: 'Second Item' },
        ]
});
```

Because we defined our knobs in the default data, we don't do so in the story.  The `withOptions` export simply calls the template function without any parameters, which will cause the default data to load.

We also have an additional story `withoutImage` which passes hard-coded data to the view.  This can be useful as specific examples.  If there are samples provided by UX, it is good to at the very least showcase the exact sample from the designs.  If there are other examples that are good to showcase, add additional stories to demonstrate these examples.


## Layouts, Pages, Placeholders, And Scaffolds
The purpose of Layouts, Placeholders, and Scaffolds is to ensure that the structure of the front-end and back-end are closely aligned to avoid integration issues where things don't map one to one. 

### Layouts
Most projects will only have a single layout, possibly one per theme.
These live in the `html/layouts` folder in a particular theme.  This should contain the required HTML structure including with `<head>` and `<body>` tags, but should otherwise be pretty sparse.  Only markup that is relevant to every page on the site should live here.

Inside the body, there should be spots for the header, content, and footer as below.  This uses the inline partial functionality (`header`, `content` and `footer` don't map to actual files).  A default value can be passed in as the body (in the case below, the default header and footer partials are added).  Note when there a partial has a body, it starts with `{{#>` instead of `{{>` and has a closing tag.

```hbs
    {{#>header}}
    {{> components/global-components/header/header }}
    {{/header}}

    <!-- END NOINDEX -->
    <main role="main">
        {{#>content}} {{/content}}
    </main>
    <!-- BEGIN NOINDEX -->
    {{#>footer}}
    {{> components/global-components/footer/footer }}
    {{/footer}}
```

### Pages
Pages will inherit from the layouts, using the partial syntax again as below.

The inline partials from the layout can be overridden with `{{#*inline` like so:
```hbs
{{#*inline "header"}}
    {{> components/global-components/header/header title="Home Header" }}
{{/inline}}
```

Here we are still passing in the Header component, but we are overriding the `title` property.  We could pass in a completely different component, or no component at all and just have the header be defined inline


If we are defining a specific page with a static list of components, we can do so as below:
```hbs
{{#> html/layouts/default }}
    {{#*inline "header"}}
        {{> components/global-components/header/header title="Home Header" }}
    {{/inline}}
    {{#*inline "content"}}
        <div class="container">
            <div class="row">
                <div class="col">
                    {{> components/independent-components/callout/callout  }}
                    {{> components/independent-components/dropdown/dropdown }}
                </div>
            </div>
        </div>
    {{/inline}}
{{/html/layouts/default}}
```

For a more general page, we'll want to use placeholders.  The `index.hbs` adds the `grid-placeholder`.  

```hbs
{{#> html/layouts/default }}

{{#*inline "header"}}
{{> components/global-components/header/header header}}
{{/inline}}

{{#*inline "content"}}
<div class="container">
    {{> html/placeholder/grid-placeholder body}}
</div>
{{/inline}}

{{#*inline "footer"}}
{{> components/global-components/footer/footer footer}}
{{/inline}}

{{/html/layouts/default}}
```

You'll notice an additional parameter passed to the partials.  This can come from a statically defined story like below.  If a property is null or undefined, the default data will be used.

```js    
{
    header: {
        title: 'Fixed Header'
    },
    body: {
        // Simplified
        components: ['eight-four'],
    }
}
```


### Placeholders
Placeholders allow for putting varying components into a part of a page.  Depending on the placeholder, it allows different components to be placed inside.  

Placeholders are more of a conceptual thing we implemented in Handlebars to better map to how components are developed and placed in the back-end.  UX should define what components can go where, but make sure that FED and BED are aligned on what the placeholders are and how they are used.  A unique placeholder should be created for each set of components allowed on a page.

We'll first talk about the Grid Placeholder which is a bit of a unique entry in that it doesn't directly add Components, but rather it adds Scaffolds for various Bootstrap grids.  But the concept is the same.  See Scaffolds below for details on Scaffolds.

`grid-placeholder.hbs`
```hbs
{{#default-data "./grid-placeholder.data.js"}}
{{#> html/placeholder/helpers/render-components}}

{{#if (equal componentName 'full-width')}}
{{> html/scaffolds/full-width }}
{{/if}}

{{#if (equal componentName 'half-width')}}
{{> html/scaffolds/half-width }}
{{/if}}

{{#if (equal componentName 'four-eight')}}
{{> html/scaffolds/four-eight  }}
{{/if}}

{{#if (equal componentName 'eight-four')}}
{{> html/scaffolds/eight-four  }}
{{/if}}

{{/html/placeholder/helpers/render-components}}
{{/default-data}}
```

This lists out the allowed components (in this case Scaffolds) that are allowed in this placeholder, as well as which partials they map to.  There is a helper `html/placeholder/helpers/render-components` that is used to loop through the selected components, as well as render out the allowed values, and support highlighting of a placeholder in the UI with a checkbox setting.

In addition to the template, the following is also needed:

`grid-placeholder.data.js`
```js
import { getPlaceholderData } from './helpers/placeholder-utils';

const allowedComponents = {
    'Full Width Grid': 'full-width',
    'Half Width Grid': 'half-width',
    'One Thirds | Two Third Grid': 'four-eight',
    'Two Thirds | One Third Grid': 'eight-four',
};
const defaultComponents = ['eight-four'];

export default (placeholderArray) => {
    return getPlaceholderData(allowedComponents, defaultComponents, placeholderArray, 'Grid');
};
```
This specifies the default components, as well as the allowed ones.  The final parameter to `getPlaceholderData` is an optional one to override the label in the Storybook knob.  Normally it is labeled as "Components" but in this case we want to call it "Grid".  This has no impact on the final page functionality, just what shows up in the storybook UI.

Let's look at a "normal" placeholder:

`src\_common\html\placeholder\side-content.hbs`
```hbs
{{#default-data "./side-content.data.js" "[Side Content]"}}
{{#> html/placeholder/helpers/render-components}}

{{#if (equal componentName 'dropdown')}}
{{> components/independent-components/dropdown/dropdown }}
{{/if}}

{{/html/placeholder/helpers/render-components}}
{{/default-data}}
```
`src\_common\html\placeholder\side-content.data.js`
```js
import { getPlaceholderData } from './helpers/placeholder-utils';

const allowedComponents = { 'Dropdown': 'dropdown'};

const defaultComponents = ['dropdown'];

export default (placeholderArray) => {    
    return getPlaceholderData(allowedComponents, defaultComponents, placeholderArray);
};
```

The Side Content placeholder in this case only allows the `dropdown` component.  One difference to note is that "[Side Content]" is passed to the `#default-data` helper.  This is an optional parameter to indicates a new placeholder scope.  In the Storybook Knobs, settings for components within this placeholder have "[Side Content]" applied to the name of the tab which holds the settings.  Depending on how it's used, this may or may not be desireable.  The `tab-content` for example does not specify a placeholder name, because the Tabbed Container component itself specifies it to be different per tab.

### Scaffolds
Scaffolds are just various grid layouts that are available to choose from.  They may be simple or they may be complex, depending on the requirements of the project.  UX should define the various configurations that are allowed, and FED and BED should be aligned on these layouts.

## Misc

### SVG to Font Icons
SVG files intended to be combined/coverted to a font should be placed in the `svg-to-font` folder in theme root.

With `iconfont-webpack-plugin` your css has now a new feature: `font-icon` declarations
```css
a:before {
  font-icon: url('./account.svg');
  transition: 0.5s color;
}

a:hover:before {
  color: red;
}
```
and it will be compiled into:
```css
@font-face {
  font-family: i96002e;
  src: url("data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAA.....IdAA==") format('woff');
}

a:before {
  font-family: i96002e;
  content: '\E000';
  transition: 0.5s color;
}

a:hover:before {
  color: red;
}
```